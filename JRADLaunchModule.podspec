#
#  Be sure to run `pod spec lint JRTest.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

s.name             = "JRADLaunchModule"
s.version          = "1.0.0"
s.summary          = "JRADLaunchModule"

s.description      = <<-DESC
JRADLaunchModule广告启动页
        DESC

s.homepage         = "http://git.oschina.net/dsdj/JRADLaunchModule"

s.license          = 'MIT'
s.author           = { "NirvanAcN" => "mahaomeng@gmail.com" }
s.source           = { :git => "https://git.oschina.net/dsdj/JRADLaunchModule.git", :tag => s.version.to_s }
s.social_media_url = 'http://weibo.com/2743943525'

s.ios.deployment_target = '8.0'
s.platform      = :ios, '8.0'
s.source_files  = 'JRADLaunchModule/Needed/**/*'
#s.resources     = 'JRModuleManager/Resource/**/*'

s.frameworks = 'UIKit'

s.dependency 'SDWebImage'
s.dependency 'SnapKit'
s.dependency 'JRTimer'

end
