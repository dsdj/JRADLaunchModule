//
//  ADLaunchable.swift
//  JRADLaunchModule
//
//  Created by 京睿 on 2017/5/22.
//  Copyright © 2017年 JingRuiWangKe. All rights reserved.
//

import Foundation

public let loadingKey = "jr_loading"

/// ADLaunchable
public protocol ADLaunchable {
    func jump()
    func exit()
}

///// ADLaunchableActionDelegate
//public protocol ADLaunchableActionDelegate: NSObjectProtocol {
//    func launch(didSelect manager: ADLaunchable)
//}
//
//extension ADLaunchableActionDelegate {
//    func launch(didSelect manager: ADLaunchable) { }
//}

/// ADLaunchableConfigDelegate
public protocol ADLaunchableConfigDelegate: NSObjectProtocol {
    /// Return the disappearance time interval
    ///
    /// - Parameter manager: ADLaunchable
    /// - Returns: time interval
    func launch(countdownFor manager: ADLaunchable) -> TimeInterval
    
    func launch(imageUrlFor manager: ADLaunchable) -> String
    
    /// Action, on launch image did selected
    ///
    /// - Parameter manager: ADLaunchable
    func launch(didSelect manager: ADLaunchable)
    
    /// Aciton, on launch image exit
    ///
    /// - Parameter manager: manager description
    func launch(exit manager: ADLaunchable)
    
    func launch(canShow manager: ADLaunchable, launch: UIViewController)
}

extension ADLaunchableConfigDelegate {
    func launch(countdownFor manager: ADLaunchable) -> TimeInterval {
        return 5
    }
    
    func launch(didSelect manager: ADLaunchable) { }
    
    func launch(exit manager: ADLaunchable) { }
}
