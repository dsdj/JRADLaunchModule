//
//  DetailViewController.swift
//  JRADLaunchModule
//
//  Created by 京睿 on 2017/5/22.
//  Copyright © 2017年 JingRuiWangKe. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = UIColor.red
        
        let btn = UIButton.init(type: .system)
        btn.backgroundColor = UIColor.white
        btn.addTarget(self, action: #selector(exit), for: .touchUpInside)
        view.addSubview(btn)
        
        btn.snp.makeConstraints {
            $0.bottom.trailing.equalToSuperview()
            $0.width.height.equalTo(100)
        }
    }


    func exit() {
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
